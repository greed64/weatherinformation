﻿using Azure.Storage.Blobs;
using System.Threading.Tasks;

namespace WeatherInformation.DataAccess.Repositories
{
    public interface IWeatherInformationRepository
    {
        Task<BlobContainerClient> GetBlob();
    }
}