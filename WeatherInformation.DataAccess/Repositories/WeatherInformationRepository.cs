﻿using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using WeatherInformation.DataAccess.Conections;

namespace WeatherInformation.DataAccess.Repositories
{
    public class WeatherInformationRepository : IWeatherInformationRepository
    {
        private readonly IWeatherInformationBlob _weatherInformationBlob;

        public WeatherInformationRepository(IWeatherInformationBlob weatherInformationBlob)
        {
            _weatherInformationBlob = weatherInformationBlob;
        }

        //TODO: Only for tests
        public async Task<BlobContainerClient> GetBlob()
        {
            BlobContainerClient blobContainer = await _weatherInformationBlob.GetContainerClient();

            BlobClient blobClient = blobContainer.GetBlobClient("metadata.csv");
            BlobClient blobClient2 = blobContainer.GetBlobClient(@"dockan\rainfall\2019-01-10.csv");

            BlobDownloadInfo blobDownloadInfo = await blobClient.DownloadAsync();
            BlobDownloadInfo blobDownloadInfo2 = await blobClient2.DownloadAsync();

            //BlobInfo blobInfo = new BlobInfo(blobDownloadInfo.Content, blobDownloadInfo.ContentType);

            var blobItems = new List<string>();

            await foreach (BlobItem blobItem in blobContainer.GetBlobsAsync())
            {
                blobItems.Add(blobItem.Name);
            }

            return blobContainer;
        }
    }
}