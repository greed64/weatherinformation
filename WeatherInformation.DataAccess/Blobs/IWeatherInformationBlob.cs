﻿using WeatherInformation.DataAccess.Blobs;

namespace WeatherInformation.DataAccess.Conections
{
    public interface IWeatherInformationBlob : IBlob
    {
    }
}