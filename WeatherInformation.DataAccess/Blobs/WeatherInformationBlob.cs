﻿using Azure.Storage.Blobs;
using System.Threading.Tasks;
using WeatherInformation.BlobConnection.Conections;

namespace WeatherInformation.DataAccess.Conections
{
    class WeatherInformationBlob : IWeatherInformationBlob
    {
        private readonly IAzureBlobConnection _azureBlobConnection;

        //TODO: consider get from 
        private const string BlobContainerName = "WetherInformationBlob";
        private static readonly object padlock = new object();
        private BlobContainerClient _blobContainerClient;

        public WeatherInformationBlob(IAzureBlobConnection azureBlobConnection)
        {
            _azureBlobConnection = azureBlobConnection;
        }

        //TODO: add try catch and log
        public async Task<BlobContainerClient> GetContainerClient()
        {
            lock (padlock)
            {
                if (_blobContainerClient != null)
                    return _blobContainerClient;

                //TODO: consider lock/ async
                //_blobContainerClient = await _azureBlobConnection.GetBlobContainerClient(BlobContainerName);
                _blobContainerClient = _azureBlobConnection.GetBlobContainerClient(BlobContainerName).Result;
            }
            return _blobContainerClient;
        }
    }
}