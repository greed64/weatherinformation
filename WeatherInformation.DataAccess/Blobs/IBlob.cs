﻿using Azure.Storage.Blobs;
using System.Threading.Tasks;

namespace WeatherInformation.DataAccess.Blobs
{
    public interface IBlob
    {
        Task<BlobContainerClient> GetContainerClient();
    }
}