﻿using Autofac;
using System.Reflection;
using WeatherInformation.BlobConnection.Modules;

namespace WeatherInformation.DataAccess.Modules
{
    public class DataAccessIoCModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();

            builder.RegisterAssemblyTypes(assembly)
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterModule<BlobConnectionIoCModule>();

            base.Load(builder);
        }
    }
}