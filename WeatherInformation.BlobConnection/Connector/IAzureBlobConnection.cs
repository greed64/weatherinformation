﻿using Azure.Storage.Blobs;
using System.Threading.Tasks;

namespace WeatherInformation.BlobConnection.Conections
{
    public interface IAzureBlobConnection
    {
        Task<BlobContainerClient> GetBlobContainerClient(string blobContainerName);
    }
}