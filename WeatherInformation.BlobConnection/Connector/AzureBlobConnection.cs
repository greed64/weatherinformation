﻿using Azure.Storage.Blobs;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;

namespace WeatherInformation.BlobConnection.Conections
{
    public class AzureBlobConnection : IAzureBlobConnection
    {
        private readonly IConfiguration _configuration;

        private static readonly object padlock = new object();
        private BlobServiceClient _blobServiceClient;

        public AzureBlobConnection(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        //TODO: add try catch and log
        public async Task<BlobContainerClient> GetBlobContainerClient(string blobContainerName)
        {
            var containerName = _configuration.GetSection("Blobs").GetSection(blobContainerName).Value;

            if (string.IsNullOrWhiteSpace(containerName))
            {
                throw new ArgumentException("Configuration must contain ContainerName");
            }

            var blobServiceClient = GetBlobServiceClient();

            //TODO: Make async
            var blobContainerClient = blobServiceClient.GetBlobContainerClient(containerName);

            var isContainerExist = await blobContainerClient.ExistsAsync();
            if (!isContainerExist.Value)
            {
                throw new ArgumentException("Container doesn't exist");
            }

            return blobContainerClient;
        }

        private BlobServiceClient GetBlobServiceClient()
        {
            lock (padlock)
            {
                if (_blobServiceClient != null)
                    return _blobServiceClient;

                var storageConnectionString = _configuration.GetConnectionString("BlobConnection");
                if (string.IsNullOrWhiteSpace(storageConnectionString))
                {
                    throw new ArgumentException("Configuration must contain StorageConnectionString");
                }

                _blobServiceClient = new BlobServiceClient(storageConnectionString);
            }
            return _blobServiceClient;
        }

    }
}