﻿using Autofac;
using System.Reflection;

namespace WeatherInformation.BlobConnection.Modules
{
    public class BlobConnectionIoCModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();

            builder.RegisterAssemblyTypes(assembly)
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            base.Load(builder);
        }
    }
}