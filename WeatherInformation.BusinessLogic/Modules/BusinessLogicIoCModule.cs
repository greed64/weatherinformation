﻿using Autofac;
using System.Reflection;
using WeatherInformation.DataAccess.Modules;

namespace WeatherInformation.BusinessLogic.Modules
{
    public class BusinessLogicIoCModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();

            builder.RegisterAssemblyTypes(assembly)
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterModule<DataAccessIoCModule>();

            base.Load(builder);
        }
    }
}