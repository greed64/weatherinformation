﻿using System.Threading.Tasks;
using WeatherInformation.DataTransferObject.Inbound;
using WeatherInformation.DataTransferObject.Outbound;

namespace WeatherInformation.BusinessLogic.Services
{
    public interface IWeatherInformationService
    {
        Task TestBlob();
        Task<WeatherInformationMeasurements> GetAllMeasurements(WeatherInformationQuery query);
        Task<WeatherInformationMeasurements> GetUnitMeasurements(WeatherInformationQuery query);
    }
}