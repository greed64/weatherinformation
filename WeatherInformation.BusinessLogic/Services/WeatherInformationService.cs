﻿using System.Threading.Tasks;
using WeatherInformation.DataAccess.Repositories;
using WeatherInformation.DataTransferObject.Inbound;
using WeatherInformation.DataTransferObject.Outbound;

namespace WeatherInformation.BusinessLogic.Services
{
    public class WeatherInformationService : IWeatherInformationService
    {
        private readonly IWeatherInformationRepository _weatherInformationRepository;

        public WeatherInformationService(IWeatherInformationRepository weatherInformationRepository)
        {
            _weatherInformationRepository = weatherInformationRepository;
        }

        public async Task TestBlob()
        {
            await _weatherInformationRepository.GetBlob();
        }

        public async Task<WeatherInformationMeasurements> GetAllMeasurements(WeatherInformationQuery query)
        {
            var blob = await _weatherInformationRepository.GetBlob();

            return new WeatherInformationMeasurements();
        }

        public Task<WeatherInformationMeasurements> GetUnitMeasurements(WeatherInformationQuery query)
        {
            throw new System.NotImplementedException();
        }
    }
}