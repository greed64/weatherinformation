﻿using System;
using WeatherInformation.DataAccess.Repositories;
using WeatherInformation.DataTransferObject.Inbound;

namespace WeatherInformation.BusinessLogic.Validators
{
    public class WeatherInformationQueryValidator : IWeatherInformationQueryValidator
    {
        private readonly IWeatherInformationRepository _weatherInformationRepository;

        public WeatherInformationQueryValidator(IWeatherInformationRepository weatherInformationRepository)
        {
            _weatherInformationRepository = weatherInformationRepository;
        }
        public bool Validate(WeatherInformationQuery query)
        {
            var isValid = true;

            if (!HasCorrectDate(query.Data))
            {
                ThrowAndLogError("IncorectData");

                return false;
            }

            //var metadata = _weatherInformationRepository.GetMetadata();


            return isValid;
        }

        private bool HasCorrectDate(DateTime date)
        {
            var maxDateTime = DateTime.Now;
            var minDateTime = new DateTime(1800);

            return date < maxDateTime && date > minDateTime;
        }

        private void ThrowAndLogError(string messaage)
        {

            //throw
            //log
        }
    }
}
