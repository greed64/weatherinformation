﻿using WeatherInformation.DataTransferObject.Inbound;

namespace WeatherInformation.BusinessLogic.Validators
{
    public interface IWeatherInformationQueryValidator
    {
        bool Validate(WeatherInformationQuery query);
    }
}