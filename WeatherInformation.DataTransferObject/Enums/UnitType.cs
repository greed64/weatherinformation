﻿namespace WeatherInformation.DataTransferObject.Enums
{
    public enum UnitType
    {
        Rainfall,
        Humidity,
        Temperature
    }
}