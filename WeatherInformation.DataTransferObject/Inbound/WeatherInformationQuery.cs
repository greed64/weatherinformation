﻿using System;

namespace WeatherInformation.DataTransferObject.Inbound
{
    //TODO: think about enums for device and unit
    public class WeatherInformationQuery
    {
        public string Device { get; set; }

        public DateTime Data { get; set; }

        public string Unit { get; set; }
    }
}