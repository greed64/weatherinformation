﻿using System;

namespace WeatherInformation.DataTransferObject.Outbound
{
    public class WeatherInformationDataRow
    {
        public DateTime Date { get; set; }
        public double Value { get; set; }
    }
}