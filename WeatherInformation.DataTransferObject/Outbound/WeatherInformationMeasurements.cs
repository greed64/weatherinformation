﻿using System.Collections;
using System.Collections.Generic;
using WeatherInformation.DataTransferObject.Enums;

namespace WeatherInformation.DataTransferObject.Outbound
{
    public class WeatherInformationMeasurements
    {
        public string Device { get; set; }
        //TODO: refactor name
        public Dictionary<UnitType, IEnumerable<WeatherInformationDataRow>> Results { get; set; }
        //public UnitType Unit { get; set; }

        //public IEnumerable<WeatherInformationDataRow> DataRows { get; set; }
    }
}
