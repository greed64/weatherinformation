﻿using Autofac;
using System.Reflection;
using WeatherInformation.BusinessLogic.Modules;

namespace WeatherInformation.Api.Modules
{
    public class WebApiIoCModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();

            builder.RegisterAssemblyTypes(assembly)
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<Program>().AsSelf();
            builder.RegisterType<Startup>().AsSelf();
            builder.RegisterModule<BusinessLogicIoCModule>();

            base.Load(builder);
        }
    }
}