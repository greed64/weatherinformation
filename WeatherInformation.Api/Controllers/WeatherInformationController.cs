﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using WeatherInformation.BusinessLogic.Services;
using WeatherInformation.DataTransferObject.Inbound;
using WeatherInformation.DataTransferObject.Outbound;
using WeatherInformation.WebApi.Controllers;

namespace WeatherInformation.Api.Controllers
{
    public class WeatherInformationController : BaseApiController
    {
        private readonly ILogger<WeatherInformationController> _logger;
        private readonly IWeatherInformationService _weatherInformationService;

        public WeatherInformationController(ILogger<WeatherInformationController> logger, IWeatherInformationService weatherInformationService)
        {
            _logger = logger;
            _weatherInformationService = weatherInformationService;
        }


        //TODO: Add cache
        [HttpGet("v1/devices/{device}/data/{data}/{unit?}")]

        //TODO: consider datetype 
        public async Task Get([FromRoute] WeatherInformationQuery query)
        {
            //verify query

            //return as json
            //GetAllMeasurements(query)
            //GetUnitMeasurements(query)
            //consuder two methods
            WeatherInformationMeasurements weatherMeasurements;

            if (string.IsNullOrEmpty(query.Unit))
            {
                weatherMeasurements = await _weatherInformationService.GetAllMeasurements(query);
            }
            else
            {
                weatherMeasurements = await _weatherInformationService.GetUnitMeasurements(query);
            }

            /*
             * Add new service and method
             * Add new model in BL, use query approach
             * Add validator in BL for checking query
             * handle error and different staf
             */
        }
    }
}