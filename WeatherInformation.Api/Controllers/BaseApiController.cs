﻿using Microsoft.AspNetCore.Mvc;

namespace WeatherInformation.WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BaseApiController : ControllerBase
    {
    }
}
